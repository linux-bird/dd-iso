###### todo ; ventoy installation debian ... how to.

# dd-create-live-iso-cli :+: ventoy :+: Dvdburn iso :+: Bashmount

#### Can use .ISO or .IMG


### location scripts


     /usr/local/bin/start-dd-create-live-iso-cli.sh
     /usr/local/bin/dd-create-live-iso-cli
     /usr/share/applications/dd-create-live-iso-cli.desktop
     /usr/local/bin/bashmount
     
 
 ### INSTALL dd-iso to get full experience or launch ./dd-create-live-iso-cli directly (errors) . . . but works.


 `$ start-dd-create-live-iso-cli.sh`    

  - _opens roxterm exec __dd-create-live-iso-cli__ & set roxterm geometry._

 `$ dd-create-live-iso-cli`

  - _shows list usb devices to choose from. refresh media option._
  
  - _opens --tab with bashmount._

  - _format usb to ext4, if not allready done._

  - _search for ISO files in the downloads dir with whiptail._

  - _possible to safely unmount device, when the job is done, with bashmount._

  - _close or create new iso._


##### DEPS ::: __roxterm, hwinfo, lolcat, figlet, (option) nerd-fonts-hack, bashmount, ventoy-bin.__

source > [Bashmount Github](https://github.com/jamielinux/bashmount)
---
source > [VENTOY](https://github.com/ventoy/Ventoy)
---

### EASY INSTALL

` $ ./install-all-at-once- <arch> or <debian> .sh` - Will make scripts excutable and put them in place. Also installing the Deps.

---

![dd-iso](https://codeberg.org/attachments/f8a82c46-af87-4237-a75f-e836945f498d "dd-iso")


![dd-iso](https://codeberg.org/attachments/427ba354-cdf8-43bc-9fcf-5ef0399b334a "dd-iso")

---

