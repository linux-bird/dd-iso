#!/usr/bin/env bash

chmod +x bashmount start-dd-create-live-iso-cli.sh dd-create-live-iso-cli install-deps-debian.sh

sh ./install-deps-debian.sh

sudo mv dd-create-live-iso-cli /usr/local/bin/
sudo mv start-dd-create-live-iso-cli.sh /usr/local/bin/
sudo mv dd-create-live-iso-cli.desktop /usr/share/applications/
sudo mv bashmount /usr/local/bin/

echo " FILES MOVED TO"
echo ""
echo "/usr/local/bin/start-dd-create-live-iso-cli.sh"
echo ""
echo "/usr/local/bin/dd-create-live-iso-cli"
echo ""
echo "/usr/share/applications/dd-create-live-iso-cli.desktop"
echo ""
echo "/usr/local/bin/bashmount"
echo ""


