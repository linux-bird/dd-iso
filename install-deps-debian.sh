#!/usr/bin/bash

sudo apt install whiptail
sudo apt install roxterm
sudo apt install hwinfo
sudo apt install lolcat
sudo apt install figlet
sudo apt install zenity
sudo apt install fonts-hack

exit 0
